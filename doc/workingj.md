# Arbeitsjournal Kurzfassung

#### Day 1
- Ausführungen im Plenum zu statischem Routing verfolgt
- Mittels Lernsoftware Filius theoretisches Wissen (autodidaktisch sowie via Ausführungen im Plenum angeeignet) praktisch angewendet.
- Repetitionsquiz M117 auf Day 2 erledigt
- Nutzung von Cisco Packet Tracer mit bereits privat genutzten Umgebungen ([siehe entsp. Anhänge](/media))

#### Day 2
- Ausführungen im Plenum zu Inhalten des vergangenen Moduls 117 verfolgt
- Repetitionsquize zu Unterrichtsinhalten gelöst
- Selbstständig mit Router in der Cloud (CHR/RouterOS) experimentiert

#### Day 3
- Selbstständig GNS3 auf lokalem System installiert
- Softwareimage für RouterOS zur Verwendung in GNS3 besorgt [Details](https://enatlabs.atlassian.net/l/cp/v61K6C52)

#### Day 4

#### Day 5

#### Day 6
- GitLab Repo als zentraler Ablageort eingerichtet
