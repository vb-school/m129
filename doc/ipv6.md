# IPv6 @ LAN

Link-local Addresses -> Bereits auto. festgelegt <br>
Unique-local Addresses -> fd00::/8

```
fd + [40 Bits Site ID] + [16 Bits Subnet ID]/64
fd43:4444:3333:0001::/64
```


Global Unicast -> 2000::/3

```
2001::/16 -> [RIR]
2001:0db8::/32 -> [LIR/ISP]
2001:0db8:3300::/40 -> [Enduser/Customer]
2001:0db8:3301::/48 -> Subnet 1 Customer
2001:0db8:3302::/48 -> Subnet 2 Customer
2001:0db8:3303::/48 -> Subnet 3 Customer
2001:0db8:3304::/48 -> Subnet 4 Customer
2001:0db8:3305::/48 -> Subnet 5 Customer
2001:0db8:3306::/48 -> Subnet 6 Customer
2001:0db8:3307::/48 -> Subnet 7 Customer
2001:0db8:33AA:4343:/64 -> Subnet 4343 Customer
```



